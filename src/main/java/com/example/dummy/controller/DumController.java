package com.example.dummy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DumController {

	@RequestMapping("/read")
	public String read() {
		return "hello";
	}
}
